import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:nikas_client/screens/index_screen.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          brightness: Brightness.light,
        ),
      ),
      debugShowCheckedModeBanner: false,
      home: IndexScreen(),
    );
  }
}